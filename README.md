# README #

This README documents the steps necessary to get this application up and running on the STM32F407 target board and provides some internal information on hardware configuaration.

### Summary ###

This IAR / Windows project is a modification of the C++501 exercise 10 solution, but using an Rx interrupt for the serial port. All low-level support of GPIO ports and USART3 configuration has been integrated into the startup code, so it's only necessary to configure the USART peripheral and change the I/O direction of the GPIOD pins in the application code.

### Setup ###

The setup assumes the STM32F407 target is connected to the washing simulator (WMS) board and all powered by a 9-12V PSU. Debugging / flashing is via a Segger J-Link unit. The USB port on the STM32F407 target is used for serial I/O and the J2 link is in the 'EN USB' position.

Software requirements:
IAR Embedded Workbench for ARM V6.21.2.2906
Tested in Windows 8.1
FTDI USB Serial Converter Driver 2.12.6.0

USB Issue: I have only managed to get the USB to work on one of my laptop's USB ports with no success using a USB hub.

### BSP Details ###

All the BSP files are generated from the STM32CubeMX application, however only 3 source files are included in the project:

startup_stm32f407xx.s: The main bootstrap assembly code, setting up the stack, interrupt vectors and basic system clock configuration. There is a call to SystemInit() in system_stm32f4xx.c to provide hardware configuration, before calling main.

system_stm32f4xx.c: SystemInit() provides further clock setup and this is where this is extra code to switch on the periperal clocks for GPIOB, GPIOD and USART3 in located. GPIOB is also setup so that the USART3 TX and RX pins are configured for the alternative funtions. The clock configuration has come from the STM32CubeMX tool and makes the system clock 96MHz and the periperhal bus 24MHz.The 8MHz HSE (external clock) is used.

stm32f4xx_hal_rcc.c: This has been included for the HAL_RCC_GetPCLK1Freq() function used to calculate the current peripheral clock frequency for the USART3 baud rate setup.

### NVIC ###

In Serial.cpp, a public function is provided to access the Serial class static ISR function so that linker installs the correct vector for the USART3 interrupt through the weak linkage mechanism. This is a simple way of getting it to work, but not particularly elegant. It's OK in this application as only one USART is used. As there is only one USART3 interrupt, it is assumed that only the Rx interrupt will ever be active and no priority is specified for this vector.

### Further work ###

- Resolve the issues of using the USART3 interrupt, so that both Rx and Tx interrupts can be used
- Find a more elegant way of linking differing USART ISRs with the actual vectors