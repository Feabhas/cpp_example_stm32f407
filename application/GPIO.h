
#ifndef GPIO_H
#define GPIO_H

#include <stdint.h>

namespace stm32f407
{
  class GPIO
  {
  public:
    // GPIO definitions
    //
    enum PinNumber 
    {
      Pin1 = 1,
      Pin2,
      Pin3,
      Pin4,
      Pin5,
      Pin6,
      Pin7,
      Pin8, 
      Pin9,
      Pin10,
      Pin11,
      Pin12,
      Pin13,
      Pin14,
      Pin15
    };
    
    enum PinDirection 
    {
      INPUT, 
      OUTPUT
    };
    
    enum Port
    {
      PORT_BASE_ADDRESS = 0x40020C00
    };
    
  public:
    // Public interface
    //
    explicit GPIO(uint32_t address);
    
    void set        (PinNumber num);
    void clear      (PinNumber num);
    bool isSet      (PinNumber num);
    void setAsOutput(PinNumber num);
    void setAsInput (PinNumber num);
    
  private:
    // Register structure overlay
    //
    struct regs
    {
      uint32_t moder;
      uint32_t otyper;
      uint32_t ospeedr;
      uint32_t pupdr;
      uint32_t idr;
      uint32_t odr;
      uint32_t bsrr;
      uint32_t lckr;
      uint32_t afrl;
      uint32_t afrh;
    };
    regs&  registers;
  
  private:
    // Disable copying.
    //
    GPIO(const GPIO&);
    GPIO& operator=(const GPIO&);
  };

} // namespace lpc2129

#endif //  GPIO_H
