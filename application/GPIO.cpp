#include "GPIO.h"

namespace stm32f407
{
 
  GPIO::GPIO (uint32_t address)
    : registers (*(reinterpret_cast<regs*>(address)))
  {
  }
  
  
  void GPIO::set(PinNumber num)
  {
    registers.bsrr = (1 << num);
  }
  
 
  void GPIO::clear(PinNumber num)
  {
    registers.bsrr = ((1 << num) << 16);
  }
  
  
  bool GPIO::isSet(PinNumber num)
  {
    return ((registers.idr & (1 << num)) != 0);
  }
  
  
  void GPIO::setAsOutput(PinNumber num)
  {
    registers.moder |= (1 << (num * 2));
  }
  
  
  void GPIO::setAsInput(PinNumber num)
  {
    registers.moder &= ~(3 << (num * 2));
  }
}