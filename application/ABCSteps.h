#ifndef ABC_STEPS_H
#define ABC_STEPS_H

#include "Step.h"

namespace wms
{
  class Motor;
  
  class MotorStep : public Step
  {
  public:
    MotorStep();
    MotorStep(StepNumber val, Motor& motor);
    MotorStep(StepNumber val, I_OutputNumeric& pOutputDevice, Motor& motor);
    MotorStep(StepNumber val, I_OutputNumeric& pOutputDevice, const char* str, Motor& motor);
    virtual ~MotorStep();
  
    virtual void run() const = 0;
    
  protected:
    // Protected interface to de-couple the derived
    // classes from the MotorStep's implementation
    //
    void motorOn() const;
    void motorOff() const;
    void motorChangeDir() const;
    
  private:
    Motor* pMotor;
  };

  
  
  class WashStep : public MotorStep
  {
  public:
    WashStep();
    WashStep(StepNumber val, Motor& motor);
    WashStep(StepNumber val, I_OutputNumeric& pOutputDevice, Motor& motor);
    WashStep(StepNumber val, I_OutputNumeric& pOutputDevice, const char* str, Motor& motor);
    virtual ~WashStep();
  
    virtual void run() const;
    
  private:
  };
  
  
  class RinseStep : public MotorStep
  {
  public:
    RinseStep();
    RinseStep(StepNumber val, Motor& motor);
    RinseStep(StepNumber val, I_OutputNumeric& pOutputDevice, Motor& motor);
    RinseStep(StepNumber val, I_OutputNumeric& pOutputDevice, const char* str, Motor& motor);
    virtual ~RinseStep();
  
    virtual void run() const;
    
  private:
  };
  
  
  class SpinStep : public MotorStep
  {
  public:
    SpinStep();
    SpinStep(StepNumber val, Motor& motor);
    SpinStep(StepNumber val, I_OutputNumeric& pOutputDevice, Motor& motor);
    SpinStep(StepNumber val, I_OutputNumeric& pOutputDevice, const char* str, Motor& motor);
    virtual ~SpinStep();
  
    virtual void run() const;
    
  private:
  };

} // namespace wms

#endif // ABC_STEPS_H
