#include "DebugLog.h"
#include "wms.h"

namespace wms
{
  WMS::WMS()
    : num_of_cycles(0)
  {
    for (int i = 0; i < MAX_PROGRAMMES_PER_WMS; i++)
    {
      cycle[i] = 0;
    }
  };
  
  WMS::Error WMS::add(WashProgramme &wp)
  {
    Error status = FULL;
    
    if (num_of_cycles < MAX_PROGRAMMES_PER_WMS)
    {
      cycle[num_of_cycles++] = &wp;
      status = OK;
    }
   
    return status;
  }
  
  void WMS::run(int programme_num) const
  {
    dout << "Performing cycle num: " << programme_num << endl;
    if (cycle[programme_num])
    {
      cycle[programme_num]->run();
    }
  }
}
