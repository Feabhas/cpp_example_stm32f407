#ifndef BUZZER_H
#define BUZZER_H

// Forward reference
//
namespace stm32f407 {class GPIO;}

namespace wms
{
  class Buzzer
  {
  public:
    Buzzer(stm32f407::GPIO& gpio);
    void beep();
    
  private:
    stm32f407::GPIO& port;
  };

} // namespace wms

#endif // BUZZER_H