#ifndef STEP_H
#define STEP_H

#include "Description.h"

namespace wms
{
  class I_OutputNumeric;
  
  enum StepNumber 
  {
    INVALID,
    EMPTY,
    FILL,
    HEAT,
    WASH,
    RINSE,
    SPIN,
    DRY,
    COMPLETE
  };
  
  class SevenSegment;
  
  class Step
  {
  public:
    Step();
    explicit Step(StepNumber val);
    Step(StepNumber val, I_OutputNumeric& pOutputDevice);
    Step(StepNumber val, I_OutputNumeric& pOutputDevice, const char* str);
    virtual ~Step();
  
    virtual void run() const;  
    
  private:
    StepNumber       stepNumber;
    I_OutputNumeric* pOutput;
    Description      desc;
  };

} // namespace wms

#endif // STEP_H