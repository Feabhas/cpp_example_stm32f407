#include "WashProgramme.h"
#include "Step.h"
#include <algorithm>
#include <functional>
#include "Exceptions.h"

using std::vector;
using std::for_each;
using std::mem_fun;

namespace wms
{
  // -----------------------------------------------------------------------------
  #ifdef INTERNAL_CONTAINER

  WashProgramme::WashProgramme() : nextStep(0)
  {
  }


  WashProgramme::~WashProgramme()
  {
  }


  WashProgramme::Error WashProgramme::add(Step& step)
  {
    if (nextStep > washProgrammeSize)
    {
      throw ExpectedActual("Too many steps in wash.", washProgrammeSize, nextStep);
    }
    programme.push_back(&step);
    ++nextStep;
    return OK;
  }


  void WashProgramme::run()
  {
    for_each(programme.begin(), programme.end(), mem_fun(&Step::run));
  }

  #endif // INTERNAL_CONTAINER

  // -----------------------------------------------------------------------------
  #ifdef CONTAINER_AS_PARAMETER

  WashProgramme::WashProgramme(WashProgramme::Programme& steps) : 
    programme(steps),
    nextStep(0)
  {
  }


  WashProgramme::~WashProgramme()
  {
  }


  void WashProgramme::run()
  {
    for_each(programme.begin(), programme.end(), mem_fun(&Step::run));
  }
  
  #endif // CONTAINER_AS_PARAMETER
  
} // namespace wms