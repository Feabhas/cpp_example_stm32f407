#include "Exceptions.h"
#include "DebugLog.h"
#include <string>
#include <strstream>

using std::ostrstream;

namespace wms
{
  Exception::Exception(const char* str) :
    description(str)
  {
    dout << "Exception::Exception" << endl;
  }
  
  
  Exception::~Exception()
  {
    dout << "Exception::~Exception" << endl;
  }
  
  
  Exception::Exception(const Exception& src) :
    description(src.description)
  {
    dout << "Exception::Exception(const Exception&)" << endl;
  }
  
  
  Exception& Exception::operator=(const Exception& rhs)
  {
    dout << "Exception::operator=" << endl;
    if(this != &rhs)
    {
      this->description = rhs.description;
    }
    return *this;
  }
  
  
  const char* Exception::what()
  {
    return description.asString();
  }
  
  // -----------------------------------------------------------------------------
  
  ExpectedActual::ExpectedActual(const char* str, int exp, int act) : 
    Exception(str),
    expected(exp),
    actual(act)
  {
    dout << "ExpectedActual::ExpectedActual" << endl;
  }
  
  
  ExpectedActual::~ExpectedActual()
  {
    dout << "ExpectedActual::~ExpectedActual" << endl;
  }
  
  
  ExpectedActual::ExpectedActual(const ExpectedActual& src) :
    Exception(src),
    expected(src.expected),
    actual(src.actual)
  {
    dout << "ExpectedActual::ExpectedActual(const ExpectedActual&)" << endl;
  }
  
  
  ExpectedActual& ExpectedActual::operator=(const ExpectedActual& rhs)
  {
    dout << "ExpectedActual::operator=" << endl;
    if (&rhs != this)
    {
      Exception::operator=(rhs);
      this->expected = rhs.expected;
      this->actual = rhs.actual;
    }
    return *this;
  }
  
  const char* ExpectedActual::what()
  {
    ostrstream message;
    message << Exception::what();
    message << " Expected: " << expected;
    message << " Actual: " << actual;
    message << endl;
    return message.str();
  }

} // namespace std;