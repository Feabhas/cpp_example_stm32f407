#ifndef WASHPROGRAMME_H
#define WASHPROGRAMME_H

#include <vector>

#define INTERNAL_CONTAINER
//#define CONTAINER_AS_PARAMETER

namespace wms
{
  const int washProgrammeSize = 10;
  class Step;
  
  // -----------------------------------------------------------------------------
  #ifdef INTERNAL_CONTAINER

  class WashProgramme
  {
  public:
    enum Error {OK, FULL};
  
    WashProgramme();
    ~WashProgramme();
    Error add(Step& step);
    void run();
  
  private:
    std::vector<Step*> programme;
    unsigned int nextStep;
  };

  #endif // INTERNAL_CONTAINER

  // -----------------------------------------------------------------------------
  #ifdef CONTAINER_AS_PARAMETER

  class WashProgramme
  {
  public:
    enum Error {OK, FULL};
    typedef std::vector<Step*> Programme;
  
    WashProgramme(Programme& steps);
    ~WashProgramme();
    void run();
  
  private:
    Programme& programme;
    unsigned int nextStep;
  };
  
  #endif // CONTAINER_AS_PARAMETER

} // namespace wms

#endif // WASHPROGRAMME_H