#ifndef DESCRIPTION_H
#define DESCRIPTION_H

class Description
{
public:
  Description(const char* str);
  ~Description();
  
  // Copy policy
  //
  Description(const Description& src);
  Description& operator=(const Description& rhs);
  
  const char* asString() const;
  
private:
  char* string;
};

#endif