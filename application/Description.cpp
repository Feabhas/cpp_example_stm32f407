#include "Description.h"
#include "DebugLog.h"
#include <cstring>

using std::strlen;
using std::strcpy;


Description::Description(const char* str) :
  string(new char[strlen(str) + 1])
{
  strcpy(string, str);
  //dout << "Description::Description with value: " << string << endl;
}


Description::~Description()
{
  //dout << "Description::~Description with value: " << string << endl;
  delete[] string;
}


const char* Description::asString() const
{
  return string;
}

Description::Description(const Description& src) :
  string(new char[strlen(src.string) + 1])
{
  strcpy(string, src.string);
  //dout << "Description::Description(const Description&) with value: " << string << endl;
}


Description& Description::operator=(const Description& rhs)
{
  char* temp = new char[strlen(rhs.string) + 1];
  strcpy(temp, rhs.string);
  delete this->string;
  this->string = temp;
  
  //dout << "Description::operator=(const Description&) with value: " << string << endl;
  return *this;
}