#include "Motor.h"
#include "GPIO.h"

using stm32f407::GPIO;

namespace wms
{
  Motor::Motor (GPIO& gpio) : port(gpio)
  {
    port.setAsOutput(GPIO::Pin12);
    port.setAsOutput(GPIO::Pin13);
    off();
    clockwise();
  }  
  
  void Motor::on()
  {
    port.set(GPIO::Pin12);
  }
  
  void Motor::off()
  {
    port.clear(GPIO::Pin12);
  }
  
  void Motor::clockwise()
  {
    port.clear(GPIO::Pin13);
  }
  
  void Motor::antiClockwise()
  {
    port.set(GPIO::Pin13);
  }
  
  void Motor::changeDirection()
  {
    if(port.isSet(GPIO::Pin13))
    {
      port.clear(GPIO::Pin13);
    }
    else
    {
      port.set(GPIO::Pin13);
    }
  }

} // namespace wms