#ifndef SEVENSEG_H
#define SEVENSEG_H

#include "Interfaces.h"

// Forward reference
//
namespace stm32f407 {class GPIO;}

namespace wms
{
  class SevenSegment : public I_OutputNumeric
  {
  public:
    explicit SevenSegment (stm32f407::GPIO& port);
    
    void display (unsigned int val);
    void blank ();
    
  private:
    stm32f407::GPIO& port;
    
    SevenSegment(const SevenSegment&);
    SevenSegment& operator=(const SevenSegment&);
  };

} // namespace wms

#endif // SEVENSEG_H

