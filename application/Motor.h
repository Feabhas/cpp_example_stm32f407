#ifndef MOTOR_H
#define MOTOR_H

// Forward reference
//
namespace stm32f407 {class GPIO;}

namespace wms
{
  class Motor
  {
  public:
    explicit Motor (stm32f407::GPIO& port);
    
    void on ();
    void off ();
    void clockwise ();
    void antiClockwise ();
    void changeDirection ();
    
  private:
    Motor(const Motor&);
    Motor& operator=(const Motor&);
    
    stm32f407::GPIO& port;
  };

} // namespace wms

#endif // MOTOR_H
