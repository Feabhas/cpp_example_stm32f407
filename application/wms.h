#ifndef WMS_H
#define WMS_H

#include "washprogramme.h"

#define MAX_PROGRAMMES_PER_WMS  10

namespace wms
{
  class WMS
  {
  public:
    enum Error {OK, FULL};
    
    WMS();
    Error add(WashProgramme &wp);
    void run(int programme_num) const;
    
  private:
    WashProgramme *cycle[MAX_PROGRAMMES_PER_WMS];
    int num_of_cycles;
  };
}

#endif
