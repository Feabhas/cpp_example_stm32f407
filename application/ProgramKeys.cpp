#include "ProgramKeys.h"
#include "GPIO.h"

using stm32f407::GPIO;

namespace wms
{
  ProgramKeys::ProgramKeys(GPIO& io) :
    port(io)
  {
    port.setAsInput(GPIO::Pin1);
    port.setAsInput(GPIO::Pin2);
    port.setAsInput(GPIO::Pin3);
    port.setAsInput(GPIO::Pin4);
    port.setAsInput(GPIO::Pin5);
    
    port.setAsOutput(GPIO::Pin14);
  }
  
  
  int ProgramKeys::get()
  {
    int selection = 0;
    
    setLatch();
    do
    {
      if(port.isSet(GPIO::Pin1)) selection |= 1;
      if(port.isSet(GPIO::Pin2)) selection |= 2;
      if(port.isSet(GPIO::Pin3)) selection |= 4;
      
      if (port.isSet(GPIO::Pin4)) // If CANCEL is pressed, reset
      {
        clearLatch();
        selection = 0;
        setLatch();
      }
    } while(!port.isSet(GPIO::Pin5)); // Repeat until ACCEPT key is pressed...
    
    // ...now wait for ACCEPT to be released.
    clearLatch();
    while(port.isSet(GPIO::Pin5))
    { 
    }
    
    return selection;
  }
  
  void ProgramKeys::setLatch()
  {
    port.set(GPIO::Pin14);
  }
  
  void ProgramKeys::clearLatch()
  {
    port.clear(GPIO::Pin14);
  }
  
} // namespace wms