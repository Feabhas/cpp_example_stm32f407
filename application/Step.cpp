#include "Step.h"
#include "DebugLog.h"
#include "SevenSeg.h"
#include "Interfaces.h"
#include <cstddef>

using namespace std;

namespace wms
{
  Step::Step() : 
    stepNumber(INVALID),
    pOutput(NULL),
    desc("")
  {
    //dout << "Step::Step()" << endl;
  }
  
  Step::Step(StepNumber val) :
    stepNumber(val),
    pOutput(NULL),
    desc("")
  {
    //dout << "Step::Step : Step number: " << stepNumber << endl;
  }
  
  
  Step::Step(StepNumber val, I_OutputNumeric& pOutputDevice) :
    stepNumber(val),
    pOutput(&pOutputDevice),
    desc("")
  {
    //dout << "Step::Step : Step number: " << stepNumber << endl;
  }
  
  
  Step::Step(StepNumber val, I_OutputNumeric& pOutputDevice, const char* str) :
    stepNumber(val),
    pOutput(&pOutputDevice),
    desc(str)
  {
     //dout << "Step::Step : Step number: " << stepNumber << " : ";
    //dout << desc.asString() << endl;
  }
  
  
  Step::~Step()
  {
    //dout << "Step::~Step :  Step number: " << stepNumber << endl;
  }
  
  
  void Step::run() const
  {
    //dout << "Running step " << stepNumber << " : " << desc.asString() << endl;
    if (pOutput != NULL)
    {
      pOutput->display(stepNumber);
      
      // Cross-cast to see if the output device
      // supports text output.
      //
      I_OutputText* pText = dynamic_cast<I_OutputText*>(pOutput);
      if(pText != 0)
      {
        pText->display(" - ");
        pText->display(desc.asString());
        pText->display("\n");
      }
    }
  }

} // namespace wms