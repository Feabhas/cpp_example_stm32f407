#include "DebugLog.h"
#include "Step.h"
#include "WashProgramme.h"
#include "GPIO.h"
#include "Motor.h"
#include "SevenSeg.h"
#include "Buzzer.h"
#include "ProgramKeys.h"
#include "ABCSteps.h"
#include "SerialPort.h"
#include "Exceptions.h"
#include "wms.h"


using stm32f407::GPIO;
using wms::WashProgramme;
using wms::Step;
using wms::WashStep;
using wms::RinseStep;
using wms::SpinStep;
using wms::Motor;
using wms::SevenSegment;
using wms::Buzzer;
using wms::ProgramKeys;
using wms::Exception;


// -----------------------------------------------------------------------------
//
int main()
{
  GPIO         port(GPIO::PORT_BASE_ADDRESS);
  
  Motor        motor(port);
  SevenSegment sevenSegment(port);
  Buzzer       buzzer(port);
  ProgramKeys  keys(port);
  SerialPort   term;
 
  
  WashProgramme colourWash;
  WashProgramme whiteWash;
  wms::WMS machine;
  
#define UART_IS_OUTPUT
  //#define SEVENSEGMENT_IS_OUTPUT
  
#ifdef UART_IS_OUTPUT
  Step      empty(wms::EMPTY, term, "Empty");
  Step      fill(wms::FILL, term, "Fill");
  Step      heat(wms::HEAT, term, "Heat");
  WashStep  wash(wms::WASH, term, "Wash", motor);
  RinseStep rinse(wms::RINSE, term, "Rinse", motor);
  SpinStep  spin(wms::SPIN, term, "Spin", motor);
  Step      dry(wms::DRY, term, "Dry");
  Step      complete(wms::COMPLETE, term, "Complete");
#endif
  
#ifdef SEVENSEGMENT_IS_OUTPUT
  Step      empty(wms::EMPTY, sevenSegment, "Empty");
  Step      fill(wms::FILL, sevenSegment, "Fill");
  Step      heat(wms::HEAT, sevenSegment, "Heat");
  WashStep  wash(wms::WASH, sevenSegment, "Wash", motor);
  RinseStep rinse(wms::RINSE, sevenSegment, "Rinse", motor);
  SpinStep  spin(wms::SPIN, sevenSegment, "Spin", motor);
  Step      dry(wms::DRY, sevenSegment, "Dry");
  Step      complete(wms::COMPLETE, sevenSegment, "Complete");
#endif
  
 
  try
  {
    colourWash.add(fill);
    colourWash.add(wash);
    colourWash.add(rinse);
    colourWash.add(empty);
    colourWash.add(fill);
    colourWash.add(wash);
    colourWash.add(empty);
    colourWash.add(spin);
    colourWash.add(dry);
    colourWash.add(complete);
    
    whiteWash.add(fill);
    whiteWash.add(wash);
    whiteWash.add(rinse);
    whiteWash.add(empty);
    whiteWash.add(dry);
    whiteWash.add(complete);
  }
  catch(Exception& ex)
  {
    dout << ex.what() << endl;
  }
  
  machine.add(colourWash);
  machine.add(whiteWash);
  
#ifdef SEVENSEGMENT_IS_OUTPUT
  int selection = keys.get();
#else
  int selection = term.read() - '0';
  term.write(selection + '0');
#endif
  
  machine.run(selection);
}
