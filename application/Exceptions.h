#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include "Description.h"

namespace wms
{
  class Exception
  {
  public:
    Exception(const char* str);
    ~Exception();
    Exception(const Exception& src);
    Exception& operator=(const Exception& rhs);
    
    virtual const char* what();
    
  private:
    Description description;
  };
  
  
  class ExpectedActual : public Exception
  {
  public:
    ExpectedActual(const char* str, int exp, int act);
    ~ExpectedActual();
    ExpectedActual(const ExpectedActual& src);
    ExpectedActual& operator=(const ExpectedActual& rhs);
    
    virtual const char* what();
    
  private:
    int expected;
    int actual;
  };

} // namespace wms

#endif // EXCEPTIONS_H