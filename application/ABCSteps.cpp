#include "ABCSteps.h"
#include "DebugLog.h"
#include "Motor.h"

static void wait()
{
  for(volatile int i = 0; i < 5000000; ++i);
}

//------------------------------------------------------------------------------
//
namespace wms
{
  MotorStep::MotorStep() : 
    Step(),
    pMotor(0)
  {
    //dout << "MotorStep::MotorStep" << endl;
  }
  
  
  MotorStep::MotorStep(StepNumber val, Motor& motor) :
    Step(val),
    pMotor(&motor)
  {
    //dout << "MotorStep::MotorStep : Step number: " << val << endl;
  }
  
  
  MotorStep::MotorStep(StepNumber val, I_OutputNumeric& pOutputDevice, Motor& motor) :
    Step(val, pOutputDevice),
    pMotor(&motor)
  {
    //dout << "MotorStep::MotorStep : Step number: " << val << endl;
  }
  
  
  MotorStep::MotorStep(StepNumber val, I_OutputNumeric& pOutputDevice, const char* str, Motor& motor) :
    Step(val, pOutputDevice, str),
    pMotor(&motor)
  {
    //dout << "MotorStep::MotorStep : Step number: " << val << " : ";
    //dout << str << endl;
  }
    
  
  MotorStep::~MotorStep()
  {
    //dout << "MotorStep::~MotorStep" << endl;
  }
  
  
  void MotorStep::motorOn() const
  {
    pMotor->on();
  }
  
  
  void MotorStep::motorOff() const
  {
    pMotor->off();
  }
  
  
  void MotorStep::motorChangeDir() const
  {
    pMotor->changeDirection();
  }

} // namespace wms

//------------------------------------------------------------------------------
//
namespace wms
{
  WashStep::WashStep() : 
    MotorStep()
  {
    //dout << "WashStep::WashStep" << endl;
  }
  
  
  WashStep::WashStep(StepNumber val, Motor& motor) :
   MotorStep(val, motor)
  {
    //dout << "WashStep::WashStep : Step number: " << val << endl;
  }
  
  
  WashStep::WashStep(StepNumber val, I_OutputNumeric& pOutputDevice, Motor& motor) :
    MotorStep(val, pOutputDevice, motor)
  {
    //dout << "WashStep::WashStep : Step number: " << val << endl;
  }
  
  
  WashStep::WashStep(StepNumber val, I_OutputNumeric& pOutputDevice, const char* str, Motor& motor) :
    MotorStep(val, pOutputDevice, str, motor)
  {
    //dout << "WashStep::WashStep : Step number: " << val << " : ";
    //dout << str << endl;
  }
    
  
  WashStep::~WashStep()
  {
    //dout << "WashStep::~WashStep" << endl;
  }
  
  
  void WashStep::run() const
  {
    Step::run();
    motorOn();
    wait();
    motorChangeDir();
    wait();
    motorOff();
  }
  

} // namespace wms

//------------------------------------------------------------------------------
//
namespace wms
{
  RinseStep::RinseStep() : 
    MotorStep()
  {
    //dout << "RinseStep::RinseStep" << endl;
  }
  
  
  RinseStep::RinseStep(StepNumber val, Motor& motor) :
    MotorStep(val, motor)
  {
    //dout << "RinseStep::RinseStep : Step number: " << val << endl;
  }
  
  
  RinseStep::RinseStep(StepNumber val, I_OutputNumeric& pOutputDevice, Motor& motor) :
    MotorStep(val, pOutputDevice, motor)
  {
    //dout << "RinseStep::RinseStep : Step number: " << val << endl;
  }
  
  
  RinseStep::RinseStep(StepNumber val, I_OutputNumeric& pOutputDevice, const char* str, Motor& motor) :
    MotorStep(val, pOutputDevice, str, motor)
  {
    //dout << "RinseStep::RinseStep : Step number: " << val << " : ";
    //dout << str << endl;
  }
    
  
  RinseStep::~RinseStep()
  {
    //dout << "RinseStep::~RinseStep" << endl;
  }
  
  
  void RinseStep::run() const
  {
    Step::run();
    for(int i = 0; i < 4; ++i)
    {
      motorOn();
      wait();
      motorOff();
      wait();
    }
  }
} // namespace wms
//------------------------------------------------------------------------------
//
namespace wms
{
  SpinStep::SpinStep() : 
    MotorStep()
  {
    //dout << "SpinStep::SpinStep" << endl;
  }
  
  
  SpinStep::SpinStep(StepNumber val, Motor& motor) :
    MotorStep(val, motor)
  {
    //dout << "SpinStep::SpinStep : Step number: " << val << endl;
  }
  
  
  SpinStep::SpinStep(StepNumber val, I_OutputNumeric& pOutputDevice, Motor& motor) :
    MotorStep(val, pOutputDevice, motor)
  {
    //dout << "SpinStep::SpinStep : Step number: " << val << endl;
  }
  
  
  SpinStep::SpinStep(StepNumber val, I_OutputNumeric& pOutputDevice, const char* str, Motor& motor) :
    MotorStep(val, pOutputDevice, str, motor)
  {
    //dout << "SpinStep::SpinStep : Step number: " << val << " : ";
    //dout << str << endl;
  }
    
  
  SpinStep::~SpinStep()
  {
    //dout << "SpinStep::~SpinStep" << endl;
  }
  
  
  void SpinStep::run() const
  {
    Step::run();
    motorOn();
    wait();
    wait();
    motorOff();
  }

} // namespace wms