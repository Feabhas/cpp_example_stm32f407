#ifndef INTERFACES_H
#define INTERFACES_H

namespace wms
{
  // Interface for any class capable of outputting
  // (displaying) integers.
  //
  class I_OutputNumeric
  {
  public:
    virtual void display(unsigned int value) = 0;
  };
  
  
  // Interface for any class capable of outputting
  // text (zero-terminated character) strings.
  //
  class I_OutputText
  {
  public:
    virtual void display(const char* str) = 0;
  };

} // namespace wms

#endif // INTERFACES_H