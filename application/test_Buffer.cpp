#include "TemplateBuffer.h"
#include "DebugLog.h"

int main()
{
  // ---------------------------------------------------------------------------
  // Template buffer test code
  //

  typedef Buffer<int> CircularBuffer;
  
  CircularBuffer buffer;
  
  CircularBuffer::Error err;
  
  for (int i = 0; i < 10; ++i)
  {
    err = buffer.add(i);
    if (err == CircularBuffer::OK)
    {
      dout << "OK" << endl;
    }
    else
    {
      dout << "Buffer full" << endl;
    } 
  }
  
  for (int i = 0; i < 10; ++i)
  {
    int data;
    err = buffer.get(data);
    if (err == CircularBuffer::OK)
    {
      dout << "OK - ";
      dout << "Data is: " << data << endl;
    }
    else
    {
      dout << "EMPTY - ";
      dout << "Data is: " << data << endl;
    }
  }
}