#ifndef DERIVED_STEPS_H
#define DERIVED_STEPS_H

#include "Step.h"

namespace wms
{
  class Motor;
  
  class WashStep : public Step
  {
  public:
    WashStep();
    WashStep(StepNumber val, Motor& motor);
    WashStep(StepNumber val, SevenSegment& pOutputDevice, Motor& motor);
    WashStep(StepNumber val, SevenSegment& pOutputDevice, const char* str, Motor& motor);
    virtual ~WashStep();
  
    virtual void run() const;
    
  private:
    Motor* pMotor;
  };
  
  
  class RinseStep : public Step
  {
  public:
    RinseStep();
    RinseStep(StepNumber val, Motor& motor);
    RinseStep(StepNumber val, SevenSegment& pOutputDevice, Motor& motor);
    RinseStep(StepNumber val, SevenSegment& pOutputDevice, const char* str, Motor& motor);
    virtual ~RinseStep();
  
    virtual void run() const;
    
  private:
    Motor* pMotor;
  };
  
  
  class SpinStep : public Step
  {
  public:
    SpinStep();
    SpinStep(StepNumber val, Motor& motor);
    SpinStep(StepNumber val, SevenSegment& pOutputDevice, Motor& motor);
    SpinStep(StepNumber val, SevenSegment& pOutputDevice, const char* str, Motor& motor);
    virtual ~SpinStep();
  
    virtual void run() const;
    
  private:
    Motor* pMotor;
  };

} // namespace wms
#endif
