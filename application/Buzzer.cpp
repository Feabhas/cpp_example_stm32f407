#include "Buzzer.h"
#include "GPIO.h"

using stm32f407::GPIO;

namespace wms
{
  static void wait()
  {
    for(volatile int i = 0; i < 500000; ++i);
  }
  
  
  Buzzer::Buzzer(GPIO& gpio) : port(gpio)
  {
    port.setAsOutput(GPIO::Pin15);
    port.clear(GPIO::Pin15);
  }
  
  
  void Buzzer::beep()
  {
    port.set(GPIO::Pin15);
    wait();
    port.clear(GPIO::Pin15);
  }

} // namespace wms