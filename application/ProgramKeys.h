#ifndef PROGRAMKEYS_H
#define PROGRAMKEYS_H


// Forward reference
//
namespace stm32f407 {class GPIO;}

namespace wms
{
  class ProgramKeys
  {
  public:
    ProgramKeys(stm32f407::GPIO& io);
    int get();
    
  private:
    void setLatch();
    void clearLatch();
    
    stm32f407::GPIO& port;
  };

} // namespace wms

#endif // PROGRAMKEYS_H