#include "derivedSteps.h"
#include "DebugLog.h"
#include "Motor.h"


static void wait()
{
  for(volatile int i = 0; i < 5000000; ++i);
}
//------------------------------------------------------------------------------
//
namespace wms
{
  WashStep::WashStep() : 
    Step(),
    pMotor(0)
  {
    dout << "WashStep::WashStep" << endl;
  }
  
  
  WashStep::WashStep(StepNumber val, Motor& motor) :
    Step(val),
    pMotor(&motor)
  {
    dout << "WashStep::WashStep : Step number: " << val << endl;
  }
  
  
  WashStep::WashStep(StepNumber val, SevenSegment& pOutputDevice, Motor& motor) :
    Step(val, pOutputDevice),
    pMotor(&motor)
  {
    dout << "WashStep::WashStep : Step number: " << val << endl;
  }
  
  
  WashStep::WashStep(StepNumber val, SevenSegment& pOutputDevice, const char* str, Motor& motor) :
    Step(val, pOutputDevice, str),
    pMotor(&motor)
  {
    dout << "WashStep::WashStep : Step number: " << val << " : ";
    dout << str << endl;
  }
    
  
  WashStep::~WashStep()
  {
    dout << "WashStep::~WashStep" << endl;
  }
  
  
  void WashStep::run() const
  {
    Step::run();
    pMotor->on();
    wait();
    pMotor->changeDirection();
    wait();
    pMotor->off();
  }

} // namespace wms

//------------------------------------------------------------------------------
//
namespace wms
{
  RinseStep::RinseStep() : 
    Step(),
    pMotor(0)
  {
    dout << "RinseStep::RinseStep" << endl;
  }
  
  
  RinseStep::RinseStep(StepNumber val, Motor& motor) :
    Step(val),
    pMotor(&motor)
  {
    dout << "RinseStep::RinseStep : Step number: " << val << endl;
  }
  
  
  RinseStep::RinseStep(StepNumber val, SevenSegment& pOutputDevice, Motor& motor) :
    Step(val, pOutputDevice),
    pMotor(&motor)
  {
    dout << "RinseStep::RinseStep : Step number: " << val << endl;
  }
  
  
  RinseStep::RinseStep(StepNumber val, SevenSegment& pOutputDevice, const char* str, Motor& motor) :
    Step(val, pOutputDevice, str),
    pMotor(&motor)
  {
    dout << "RinseStep::RinseStep : Step number: " << val << " : ";
    dout << str << endl;
  }
    
  
  RinseStep::~RinseStep()
  {
    dout << "RinseStep::~RinseStep" << endl;
  }
  
  
  void RinseStep::run() const
  {
    Step::run();
    for(int i = 0; i < 4; ++i)
    {
      pMotor->on();
      wait();
      pMotor->off();
      wait();
    }
  }
} // namespace wms

//------------------------------------------------------------------------------
//
namespace wms
{
  SpinStep::SpinStep() : 
    Step(),
    pMotor(0)
  {
    dout << "SpinStep::SpinStep" << endl;
  }
  
  
  SpinStep::SpinStep(StepNumber val, Motor& motor) :
    Step(val),
    pMotor(&motor)
  {
    dout << "SpinStep::SpinStep : Step number: " << val << endl;
  }
  
  
  SpinStep::SpinStep(StepNumber val, SevenSegment& pOutputDevice, Motor& motor) :
    Step(val, pOutputDevice),
    pMotor(&motor)
  {
    dout << "SpinStep::SpinStep : Step number: " << val << endl;
  }
  
  
  SpinStep::SpinStep(StepNumber val, SevenSegment& pOutputDevice, const char* str, Motor& motor) :
    Step(val, pOutputDevice, str),
    pMotor(&motor)
  {
    dout << "SpinStep::SpinStep : Step number: " << val << " : ";
    dout << str << endl;
  }
    
  
  SpinStep::~SpinStep()
  {
    dout << "SpinStep::~SpinStep" << endl;
  }
  
  
  void SpinStep::run() const
  {
    Step::run();
    pMotor->on();
    wait();
    wait();
    pMotor->off();
  }
} // namespace wms