#include "SevenSeg.h"
#include "GPIO.h"

using stm32f407::GPIO;

namespace wms
{
  SevenSegment::SevenSegment (GPIO& gpio) : port(gpio) 
  {
    port.setAsOutput(GPIO::Pin8);
    port.setAsOutput(GPIO::Pin9);
    port.setAsOutput(GPIO::Pin10);
    port.setAsOutput(GPIO::Pin11);
    blank ();
  }
  
  void SevenSegment::display(uint32_t val)
  {
    port.clear(GPIO::Pin8);
    port.clear(GPIO::Pin9);
    port.clear(GPIO::Pin10);
    port.clear(GPIO::Pin11);
    
     (val & 0x01)      ? port.set(GPIO::Pin8) : port.clear(GPIO::Pin8);
    ((val & 0x02) >> 1)? port.set(GPIO::Pin9) : port.clear(GPIO::Pin9);
    ((val & 0x04) >> 2)? port.set(GPIO::Pin10) : port.clear(GPIO::Pin10);
    ((val & 0x08) >> 3)? port.set(GPIO::Pin11) : port.clear(GPIO::Pin11);
  }
  
  void SevenSegment::blank ()
  {
    display(15);
  }

} // namespace wms