#ifndef UARTBUFFER_H
#define UARTBUFFER_H

#include "TemplateBuffer.h"

//#define OBJECT_ADAPTER_BUFFER
#define CLASS_ADAPTER_BUFFER

// -----------------------------------------------------------------------------
// Object adapter implementation
//
#ifdef OBJECT_ADAPTER_BUFFER

class UARTBuffer
{
  public:
  UARTBuffer();
  void add(unsigned char ch);
  unsigned char get();
  bool isEmpty();
  
private:
  Buffer<unsigned char, 8> buffer;
};

#endif // OBJECT_ADAPTER_BUFFER


// -----------------------------------------------------------------------------
// Class adapter implementation
//
#ifdef CLASS_ADAPTER_BUFFER

class UARTBuffer : private Buffer<unsigned char, 8>
{
public:
  UARTBuffer();
  void add(unsigned char ch);
  unsigned char get();
  bool isEmpty();
  
private:
};

#endif // CLASS_ADAPTER_BUFFER


#endif // UARTBUFFER_H