#include "SerialPort.h"
#include "stm32f407xx.h"
#include <intrinsics.h>

// Static call-back pointer
//
SerialPort* SerialPort::self = 0;

SerialPort::SerialPort(UART::address addr) :
  UART(),
  buffer()
{
  // Set up 'self' static pointer
  //
  self = this;
  
  // Configure the NVIC to interrupt on Rx
  //
  enableRxInterrupt();
  
  __disable_interrupt ();
  
  NVIC_EnableIRQ(USART3_IRQn);
  
  __enable_interrupt ();
}

void SerialPort::display(unsigned int value)
{
  UART::write(value + '0');
}


void SerialPort::display(const char* str)
{
  UART::write(str);
}


unsigned char SerialPort::read() const
{
  while(buffer.isEmpty())
  {
    ; // wait while empty...
  }
  return buffer.get();
}

//-----------------------------------------------------------------------------
// Interrupt service routine
void SerialPort::ISR()
{
  unsigned char chr;
  chr = self->RXBufferRead();
  self->buffer.add(chr);
}

extern "C" {
__irq
void USART3_IRQHandler(void)
{
  SerialPort::ISR();
}
}

