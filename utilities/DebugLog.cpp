// -----------------------------------------------------------------------------
// Copyright Feabhas Ltd 2012
// -----------------------------------------------------------------------------

#include "DebugLog.h"
#include <cstdio>
#include <cstring>

// Global debug object.
//
DebugLog dout;

//-------------------------------------------------------
// FreeRTOS doesn't support character
// output to the C-Spy tool, so when the
// DebugLog is being used with the OS it
// defaults to using a NULL function - that
// is, a function that does nothing.
//
namespace
{
  int nullOutput(int chr) { return chr; }
}

#ifndef FREE_RTOS
#define WRITE_CHAR std::putchar
#else
  #define WRITE_CHAR nullOutput
#endif
//-------------------------------------------------------

DebugLog::DebugLog()
{
  // The radix is a format string
  // for use with sprintf.
  //
  radix[0] = '%';
  radix[1] = 'd';
  radix[2] = '\0';
}


DebugLog::~DebugLog() 
{
}

DebugLog& DebugLog::operator<< (const char* str)
{
  while (*str != 0)
  {
    WRITE_CHAR(*str++);
  }
  return *this;
}

DebugLog& DebugLog::operator<< (const std::uint32_t& value)
{
  char buffer[(sizeof(std::uint32_t) * 8) + 1];
  std::sprintf(buffer, radix, value);
  operator<<(buffer);
  return *this;
}

DebugLog& DebugLog::operator<< (DebugLog& (*pf)(DebugLog& os))
{
  return pf(*this);
}

DebugLog& endl(DebugLog& os)
{
  os << "\n";
  return os;
}

DebugLog& hex(DebugLog& os)
{
  os.radix[1] = 'x';
  return os;
}


DebugLog& dec(DebugLog& os)
{
  os.radix[1] = 'd';
  return os;
}

