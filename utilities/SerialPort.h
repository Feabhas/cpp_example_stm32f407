// -----------------------------------------------------------------------------
// Copyright Feabhas Ltd 2012
// -----------------------------------------------------------------------------

// SerialPort.h
// Declaration of serial UART class

#ifndef SERIAL_PORT_H
#define SERIAL_PORT_H

#include "UART.h"
#include "Interfaces.h"
#include "UARTBuffer.h"


class SerialPort :  public UART,
                    public wms::I_OutputNumeric, 
                    public wms::I_OutputText
{
public:
  SerialPort(UART::address addr = UART::TERM);
  
  static void ISR();
 
  // The following methods are inherited
  // from the base class, UART.
  //
  //  unsigned char read() const;
  //  void write (char);
  //  void write (const char*);
  //  void cls();
  
  // Interrupt-driven base class override
  //
  unsigned char read() const;
   
private:
  // Interface realisation
  //
  virtual void display(unsigned int value);
  virtual void display(const char* str);
  
  // Interrupt configuration enums
  //
//  enum interruptEnable { rxEnable = 0x01, txEnable = 0x10 };
//  enum FIFOcontrol { FIFOEnable = 0x01, rxFIFOReset = 0x02, txFIFOReset = 0x04 };
  
  // ISR-specific members
  //
  static SerialPort* self;
  
  
  mutable UARTBuffer buffer; 
};


#endif // SERIAL_PORT_H
