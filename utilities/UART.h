// -----------------------------------------------------------------------------
// Copyright Feabhas Ltd 2012
// -----------------------------------------------------------------------------

// UART.h
// Declaration of serial UART class

#ifndef UART_H
#define UART_H

#include <stdint.h>


class UART
{
public:
  enum address  { TERM = 0x40004800 };
  enum baudRate { b9600 = 9600, b38400 = 38400, b115k = 115200 };
  enum dataBits { eight = 0, nine = (1 << 12) };
  enum stopBits { one = 0, half = (1 << 12), two = (2 << 12), one_and_half = (3 << 12) };
  enum parity   { off = 0, odd = (2 << 9), even = (3 << 9) };
    
  explicit UART (address  addr = TERM, 
                 baudRate baud = b9600, 
                 dataBits db   = eight,
                 stopBits sb   = one,
                 parity   pb   = off);
    
  unsigned char read () const;
  void write (char);
  void write (const char*);
  void cls();

protected:
  uint8_t RXBufferRead() const;
  void    TXBufferWrite(uint8_t value);
  void    enableRxInterrupt();
  
private:
  // Implementation:
  //
  enum
  { 
    RXNE_FLAG = 0x20,
    TXE_FLAG = 0x80
  };

  volatile uint32_t * const pSR;
  volatile uint32_t * const pDR;
  volatile uint32_t * const pBRR;
  volatile uint32_t * const pCR1;
  volatile uint32_t * const pCR2;
  
  UART(const UART&);
  UART& operator=(const UART&);
};


#endif // UART_H
