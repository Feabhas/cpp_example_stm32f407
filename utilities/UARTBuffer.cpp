#include "UARTBuffer.h"
#include <intrinsics.h>

// -----------------------------------------------------------------------------
// Object adapter implementation
//
#ifdef OBJECT_ADAPTER_BUFFER


UARTBuffer::UARTBuffer() : buffer()
{
  buffer.flush();
}

void UARTBuffer::add(unsigned char ch)
{
  disableInterrupts();
  buffer.add(ch);
  enableInterrupts();
}

unsigned char UARTBuffer::get()
{
  unsigned char ch;
  disableInterrupts();
  buffer.get(ch);
  enableInterrupts();
  return ch;
}

bool UARTBuffer::isEmpty()
{
   bool empty;
  disableInterrupts();
  empty = buffer.isEmpty();
  enableInterrupts();
  return empty;
}


#endif // OBJECT_ADAPTER_BUFFER


// -----------------------------------------------------------------------------
// Class adapter implementation
//
#ifdef CLASS_ADAPTER_BUFFER

UARTBuffer::UARTBuffer() : Buffer()
{
  Buffer::flush();
}

void UARTBuffer::add(unsigned char ch)
{
  __disable_interrupt();
  Buffer::add(ch);
  __enable_interrupt();
}

unsigned char UARTBuffer::get()
{
  unsigned char ch;
  __disable_interrupt();
  Buffer::get(ch);
  __enable_interrupt();
  return ch;
}

bool UARTBuffer::isEmpty()
{
  bool empty;
  __disable_interrupt();
  empty = Buffer::isEmpty();
  __enable_interrupt();
  return empty;
}
#endif // CLASS_ADAPTER_BUFFER