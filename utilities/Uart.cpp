/// -----------------------------------------------------------------------------
// Copyright Feabhas Ltd 2012
// -----------------------------------------------------------------------------

// UART.cpp
// Definition of serial UART class

#include "UART.h"
#include "stm32f4xx_hal.h"

// VT100 escape sequences.
//
#define ESC "\x1B"
#define VT100_CLS  ESC##"[2J"
#define VT100_HOME ESC##"[H"

#define USART3_ENABLE 0x2000
#define RX_ENABLE     0x0004
#define TX_ENABLE     0x0008
#define RXNEIE        0x0020

// There's an issue with this FreeRTOS project that
// means the getperipheralClockFreq() function doesn't
// always return the correct frequency.
// For expidition's sake I've hard-coded the divider
// value, based on pclk = cclk = 60MHz
// Obviously, this needs resolving.
//
#ifdef FREE_RTOS
  #define PERIPHERAL_CLOCK_FREQ 60000000
#else
  #define PERIPHERAL_CLOCK_FREQ HAL_RCC_GetPCLK1Freq()
#endif

volatile uint32_t br;

UART::UART (address  addr,
            baudRate baud,
            dataBits db,
            stopBits sb,
            parity   pb) :
  pSR (reinterpret_cast<uint32_t*>(addr)),
  pDR (reinterpret_cast<uint32_t*>(addr + 0x04)),
  pBRR (reinterpret_cast<uint32_t*>(addr + 0x08)),
  pCR1 (reinterpret_cast<uint32_t*>(addr + 0x0C)),
  pCR2 (reinterpret_cast<uint32_t*>(addr + 0x10))
{
  *pBRR = UART_BRR_SAMPLING16(HAL_RCC_GetPCLK1Freq(), baud);
  *pCR1 = USART3_ENABLE | RX_ENABLE | TX_ENABLE | db | pb;
  *pCR2 = sb;
}


unsigned char UART::read () const
{
  while (!(*pSR & RXNE_FLAG))
  {
    ; // wait...
  }
  return RXBufferRead();
}
  

void UART::write (char c)
{
  while (!(*pSR & TXE_FLAG))
  {
    ; // wait...
  }
  TXBufferWrite(c);
}


void UART::write (const char* str)
{
  while (*str != '\0')
  {
    write (*str);
    str++;
  }
}

void UART::cls()
{
  // Send the VT100 escape sequence for
  // clearing the screen, then move the 
  // cursor to the top-left corner (home)
  //
  write(VT100_CLS);
  write(VT100_HOME);
}

uint8_t UART::RXBufferRead() const
{
  return static_cast<uint8_t>(*pDR);
}


void UART::TXBufferWrite(uint8_t value)
{
  *pDR = value;
}

void UART::enableRxInterrupt()
{
  *pCR1 |= RXNEIE;
}

