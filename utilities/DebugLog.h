// -----------------------------------------------------------------------------
// Copyright Feabhas Ltd 2012
// -----------------------------------------------------------------------------

#ifndef DEBUGLOG_H
#define DEBUGLOG_H

#include <cstdint>

class DebugLog
{
public:
  DebugLog();
  ~DebugLog();

  DebugLog& operator<< (const char* str);
  DebugLog& operator<< (const std::uint32_t& value);
  DebugLog& operator<< (DebugLog& (*pf)(DebugLog& os));
   
private:
  // Modifiers
  //
  friend DebugLog& endl(DebugLog& os);
  friend DebugLog& hex(DebugLog& os);
  friend DebugLog& dec(DebugLog& os);
  
  // Disable copying
  //
  DebugLog(const DebugLog&);
  DebugLog& operator= (const DebugLog& rhs);
  
  // Radix for sprintf conversion
  char radix[3];
};

// Modifiers
//
DebugLog& endl(DebugLog& os);
DebugLog& hex(DebugLog& os);
DebugLog& dec(DebugLog& os);


// Global debug object declaration
//
extern DebugLog dout;


#endif